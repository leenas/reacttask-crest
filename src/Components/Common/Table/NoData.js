import React from 'react';
import { noDataText } from '../../../utils/utils';

const NoData = ({ ...props }) => {
	if (props.loading) {
		return null;
	}
	return <div className="rt-noData">{noDataText}</div>;
};

export default NoData;
