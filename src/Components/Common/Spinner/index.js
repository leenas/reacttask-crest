import React, { Component } from 'react';

export default class SiteSpinner extends Component {
	render() {
		const { fontSize, color, className } = this.props;
		return (
			<i
				className={`fa fa-spin fa-spinner ${className}`}
				style={{
					fontSize: fontSize ? fontSize : '24px',
					color: color ? color : 'green',
				}}
				aria-hidden="true"
			></i>
		);
	}
}
