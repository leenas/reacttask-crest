import React from 'react';
import SiteSpinner from '../Spinner';

const TableLoading = ({ ...props }) => {
	if (!props.loading) {
		return null;
	}
	return (
		<div className="rt-noData">
			<SiteSpinner fontSize="24px" color="green" className="" />
		</div>
	);
};

export default TableLoading;
