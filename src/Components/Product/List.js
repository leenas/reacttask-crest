import React, { Component } from 'react';
import {
	CRow,
	CCol,
	CCard,
	CCardHeader,
	CCardBody,
	CButton,
	CNav,
	CNavItem,
	CNavLink,
	CTabs,
	CPagination,
} from '@coreui/react';
import {
	getPages,
	pageSize,
	showToast,
	isValidArray,
	_price,
} from '../../utils/utils';

import ReactTable from 'react-table-6';
import TableLoading from '../Common/Table/TableLoading';
import NoData from '../Common/Table/NoData';
import 'react-table-6/react-table.css';
import { productList } from '../../utils/product';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import '../../assets/css/style.css';
import CompareModal from './CompareModal';

class List extends Component {
	state = {
		display_type: 'table',
		activePage: 1,
		cartProducts: [],
		selectedCheckbox: [],
		isOpenCompareModal: false,
	};

	constructor(props) {
		super(props);

		this.getPagination = this.getPagination.bind(this);
	}

	componentDidMount = () => {
		this.setCart();
	};

	setCart = () => {
		this.setState({
			cartProducts: this.getProducts(),
		});
	};

	getProducts = () => {
		return JSON.parse(localStorage.getItem('cartProducts')) || [];
	};

	setProducts = (products) => {
		localStorage.setItem('cartProducts', JSON.stringify(products));
		this.setCart();
	};

	changeView = (value) => {
		this.setState({ display_type: value });
	};

	addToCart = (item) => {
		const existingCart = this.getProducts();

		const existingProduct = existingCart.find((d) => d.id === item.id);

		if (existingProduct && existingProduct.productCount + 1 > item.inventory) {
			showToast('You cannot add item more then inventory', 0);
		} else if (
			existingProduct &&
			existingProduct.productCount + 1 > item.purchaseLimit
		) {
			showToast('You cannot add more then purchase limit', 0);
		} else {
			let updateCart = [];
			if (existingProduct) {
				const filterCart = existingCart.filter((d) => d.id !== item.id);

				const updateProduct = {
					...existingProduct,
					productCount: existingProduct.productCount + 1,
				};

				updateCart = [...filterCart, updateProduct];
			} else {
				const newProduct = {
					id: item.id,
					productCount: 1,
				};
				updateCart = [...existingCart, newProduct];
			}

			this.setProducts(updateCart);
			showToast('Product Added into cart', 1);
		}
	};

	removeFromCart = (item) => {
		const existingCart = this.getProducts();
		const filteredCart = existingCart.filter((d) => d.id !== item.id);
		this.setProducts(filteredCart);
		showToast('Product removed from cart', 1);
	};

	selectedCheckbox = (id, event) => {
		const { selectedCheckbox } = this.state;

		let values = selectedCheckbox;

		if (values.indexOf(id) === -1) {
			if (values.length === 3) {
				showToast("You can't select more then three items", 0);
				return false;
			}
			values.push(id);
		} else {
			values = values.filter((d, dI) => d !== id);
		}

		this.setState({ selectedCheckbox: values });
	};

	getColumns = () => {
		const { cartProducts, selectedCheckbox } = this.state;

		return [
			{
				maxWidth: 50,
				Header: '',
				id: 'no',
				accessor: (d) => (
					<input
						type="checkbox"
						onChange={(event) => {
							this.selectedCheckbox(d.id, event);
						}}
						checked={selectedCheckbox.includes(d.id)}
					/>
				),
			},
			{
				maxWidth: 400,
				Header: 'Name',
				id: 'name',
				accessor: (d) => (
					<React.Fragment>
						{d.imgUrl && (
							<img
								alt="User"
								src={d.imgUrl}
								className="avatar mr-2"
								style={{ height: 36, width: 36, borderRadius: '100%' }}
							/>
						)}

						<span className="cust-name">{d.name}</span>
					</React.Fragment>
				),
			},

			{
				maxWidth: 400,
				className: 'text-align-center',
				Header: 'Price',
				headerClassName: 'table-header medium',
				id: 'price',
				accessor: (d) => (
					<React.Fragment>
						<span>{_price(d.price)}</span>
					</React.Fragment>
				),
			},
			{
				maxWidth: 400,
				className: 'text-align-center',
				Header: 'Inventory',
				headerClassName: 'table-header medium',
				id: 'inventory',
				accessor: (d) => (
					<React.Fragment>
						<span>{d.inventory}</span>
					</React.Fragment>
				),
			},
			{
				maxWidth: 400,
				className: 'text-align-center',
				Header: 'Limit',
				headerClassName: 'table-header medium',
				id: 'limit',
				accessor: (d) => (
					<React.Fragment>
						<span>{d.purchaseLimit}</span>
					</React.Fragment>
				),
			},
			{
				maxWidth: 400,
				className: 'text-align-center',
				Header: 'Email',
				headerClassName: 'table-header medium',
				id: 'email',
				accessor: (d) => {
					const _product = cartProducts.find((p, pI) => p.id === d.id);

					return (
						<IconButton
							aria-label={`info about ${d.name}`}
							className="icon"
							onClick={() => {
								this.addToCart(d);
							}}
						>
							<Badge badgeContent={_product?.productCount || 0} color="primary">
								<AddShoppingCartIcon />
							</Badge>
						</IconButton>
					);
				},
			},
		];
	};

	onChangePage = (currentPage) => {
		this.setState({ activePage: currentPage });
	};

	getList = () => {
		const { activePage } = this.state;
		const listData = productList();

		return listData.slice((activePage - 1) * pageSize, activePage * pageSize);
	};

	getPagination = () => {
		const { activePage = 1 } = this.state;
		const listData = productList();

		return (
			listData.length > 0 && (
				<CPagination
					align="end"
					activePage={activePage}
					pages={getPages(listData.length)}
					onActivePageChange={(activePage) => this.onChangePage(activePage)}
				/>
			)
		);
	};

	compareResult = () => {
		const { selectedCheckbox } = this.state;

		if (!selectedCheckbox.length) {
			showToast('Please select item to compare', 0);
			return false;
		} else if (selectedCheckbox.length === 1) {
			showToast('Please select at-least two items to compare', 0);
			return false;
		}
		this.toggleCompareModal();
	};

	toggleCompareModal = () => {
		this.setState({ isOpenCompareModal: !this.state.isOpenCompareModal });
	};

	render() {
		const {
			display_type,
			cartProducts,
			isOpenCompareModal,
			selectedCheckbox,
		} = this.state;
		const columns = this.getColumns();
		const listData = this.getList();

		const products = productList();

		const loading = false;

		let total = 0;

		return (
			<React.Fragment>
				<CCol xl="12" md="12">
					<CRow>
						<CCol xl="4" md="4" className="mb-3">
							<h3 className="page-title">Products</h3>
						</CCol>
					</CRow>
				</CCol>
				<CCol xl="12" md="12">
					<CCard>
						<CCardHeader>
							<CRow>
								<CCol xl="4" md="4" className="d-flex">
									<div className="input-group">
										<button
											className="btn btn-secondary"
											type="button"
											onClick={this.compareResult}
										>
											Compare
										</button>
									</div>
								</CCol>
							</CRow>
						</CCardHeader>
						<CCardBody>
							<CCol xl="12" md="12" className=" mb-4 px-0">
								<CTabs activeTab="admin">
									<CNav variant="tabs">
										<CNavItem
											onClick={() => {
												this.changeView('table');
											}}
										>
											<CNavLink data-tab="admin">Table</CNavLink>
										</CNavItem>
										<CNavItem
											onClick={() => {
												this.changeView('grid');
											}}
										>
											<CNavLink data-tab="field">Grid</CNavLink>
										</CNavItem>
									</CNav>
								</CTabs>
							</CCol>

							{display_type === 'table' ? (
								<div className="position-relative table-responsive">
									<ReactTable
										data={listData}
										columns={columns}
										loading={loading}
										defaultPageSize={pageSize}
										className="-striped -highlight"
										showPagination={false}
										sortable={false}
										multiSort={false}
										resizable={false}
										filterable={false}
										LoadingComponent={() => <TableLoading loading={loading} />}
										NoDataComponent={() => <NoData loading={loading} />}
									/>
									{this.getPagination()}
								</div>
							) : (
								<div className="position-relative table-responsive">
									<div className="root">
										<GridList cellHeight={180} className="gridList">
											{listData.map((tile) => {
												const _product = cartProducts.find(
													(p, pI) => p.id === tile.id,
												);

												return (
													<GridListTile
														key={tile.id}
														className="single-grid-list"
													>
														<img src={tile.imgUrl} alt={tile.name} />
														<GridListTileBar
															title={<span>{tile.name}</span>}
															subtitle={
																<span>Price: {_price(tile.price)}</span>
															}
															actionIcon={
																<IconButton
																	aria-label={`info about ${tile.name}`}
																	className="icon"
																	onClick={() => {
																		this.addToCart(tile);
																	}}
																>
																	<Badge
																		badgeContent={_product?.productCount || 0}
																		color="primary"
																	>
																		<AddShoppingCartIcon />
																	</Badge>
																</IconButton>
															}
														/>
													</GridListTile>
												);
											})}
										</GridList>
									</div>
									{this.getPagination()}
								</div>
							)}
						</CCardBody>
					</CCard>
				</CCol>

				<CCol xl="12" md="12">
					<CRow>
						<CCol xl="4" md="4" className="mb-3">
							<h3 className="page-title">Cart</h3>
						</CCol>
					</CRow>
				</CCol>
				<CCol xl="12" md="12">
					<CCard>
						<CCardBody>
							<div className="position-relative table-responsive">
								<table className="table">
									<thead>
										<tr>
											<th>Product</th>
											<th>Price</th>
											<th>items</th>
											<th>Total</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										{isValidArray(cartProducts) &&
											cartProducts.map((d, dI) => {
												const _product = products.find(
													(p, pI) => p.id === d.id,
												);
												const subtotal = _product.price * d.productCount;
												total += subtotal;

												return (
													_product && (
														<tr key={d.id}>
															<td>
																{_product.imgUrl && (
																	<img
																		alt="User"
																		src={_product.imgUrl}
																		className="avatar mr-2"
																		style={{
																			height: 36,
																			width: 36,
																			borderRadius: '100%',
																		}}
																	/>
																)}

																<span className="cust-name">
																	{_product.name}
																</span>
															</td>
															<td>{_price(_product.price)}</td>
															<td>{d.productCount}</td>
															<td>{_price(subtotal)}</td>
															<td>
																<CButton
																	color="danger"
																	onClick={() => {
																		this.removeFromCart(d);
																	}}
																>
																	Remove
																</CButton>
															</td>
														</tr>
													)
												);
											})}
										{isValidArray(cartProducts) && (
											<tr>
												<td colSpan="3">Total</td>
												<td>{_price(total)}</td>
											</tr>
										)}
									</tbody>
								</table>
							</div>
						</CCardBody>
					</CCard>
				</CCol>

				{isOpenCompareModal && (
					<CompareModal
						selectedIds={selectedCheckbox}
						isOpen={isOpenCompareModal}
						toggle={this.toggleCompareModal}
					/>
				)}
			</React.Fragment>
		);
	}
}

export default List;
