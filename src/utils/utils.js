import { toast } from 'react-toastify';

export const pageSize = 10;

export const noDataText = 'No Data';

export const getPages = (totalPage) => {
	return totalPage ? Math.ceil(totalPage / pageSize) : 0;
};

export const showToast = (msg, status) => {
	if (status) {
		toast.success(msg);
	} else {
		toast.error(msg);
	}
};

export const isValidArray = (data) => {
	return data && Array.isArray(data) && data.length > 0;
};

export const _price = (num) => {
	if (typeof num == 'string') num = Number(num);
	if (!num) num = 0;
	return !isEmpty(num) ? num.toFixed(2) : '';
};

export const isEmpty = (value) => {
	return (
		value === undefined || value === null || value === '' || value === 'null'
	);
};
