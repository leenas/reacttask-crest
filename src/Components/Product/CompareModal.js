import React, { Component } from 'react';
import {
	CModal,
	CModalHeader,
	CModalBody,
	CRow,
	CCol,
	CCard,
	CCardBody,
} from '@coreui/react';
import { isValidArray, _price } from '../../utils/utils';
import { productList } from '../../utils/product';

export default class CompareModal extends Component {

	getFilteredProduct = () => {
		const { selectedIds } = this.props;
		console.log('TCL ~ CompareModal ~ selectedIds', selectedIds);
		return productList().filter((d, dI) => selectedIds.includes(d.id));
	};

	render() {
		const { isOpen, toggle } = this.props;

		const productsList = this.getFilteredProduct();

		return (
			<CModal show={isOpen} onClose={toggle} size="lg">
				<CModalHeader closeButton>Compare</CModalHeader>
				<CModalBody>
					<CRow>
						<CCol xl="12" md="12">
							<CCard>
								<CCardBody>
									<div className="position-relative table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Product</th>
													<th>Price</th>
													<th>Inventory</th>
												</tr>
											</thead>
											<tbody>
												{isValidArray(productsList) &&
													productsList.map((d, dI) => {
														return (
															<tr>
																<td>
																	{d.imgUrl && (
																		<img
																			alt="User"
																			src={d.imgUrl}
																			className="avatar mr-2"
																			style={{
																				height: 36,
																				width: 36,
																				borderRadius: '100%',
																			}}
																		/>
																	)}

																	<span className="cust-name">{d.name}</span>
																</td>
																<td>{_price(d.price)}</td>
																<td>{d.inventory}</td>
															</tr>
														);
													})}
											</tbody>
										</table>
									</div>
								</CCardBody>
							</CCard>
						</CCol>
					</CRow>
				</CModalBody>
			</CModal>
		);
	}
}
